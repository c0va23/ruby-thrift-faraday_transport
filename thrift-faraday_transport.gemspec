# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'thrift-faraday_transport'
  spec.version       = File.read(__dir__ + '/VERSION').strip
  spec.authors       = ['Dmitrij Fedorenko']
  spec.email         = ['<c0va23@gmail.com>']

  spec.summary       = 'Thrift HTTP transport'
  spec.description   = 'Thrift HTTP transport over Faraday'
  spec.homepage      = 'https://gitlab.com/c0va23/ruby-thrift-faraday-transport'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 2.4'

  spec.add_development_dependency 'bundler', '>= 2.1.4'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.82.0'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.39.0'
  spec.add_development_dependency 'yard', '~> 0.9.11'

  spec.add_dependency 'faraday', '~> 1.0.0'
  spec.add_dependency 'thrift', '>= 0.9', '<= 0.13'

  spec.metadata['yard.run'] = 'yard'
end
